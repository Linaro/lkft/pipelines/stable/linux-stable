# Description
This repository contains the pipeline files to test linux-stable
kernel and tools on the latest released kernel.

# Dependencies
Linaro/lkft/pipelines/common that needs to be included into gitlab-ci.yml file
and also a few local files gets included.

# Usage
The goal is to only change the builds.yml, tests.yml, sanity_job.yml and
tuxconfig.yml files, also the variables in the gitlab-ci.yml file.
